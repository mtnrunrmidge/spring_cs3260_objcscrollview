//
//  main.m
//  ObjC_ScrollviewApp
//
//  Created by Margaret Schroeder on 1/12/18.
//  Copyright © 2018 Margaret Schroeder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
