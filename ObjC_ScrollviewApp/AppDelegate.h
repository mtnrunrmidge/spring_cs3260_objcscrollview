//
//  AppDelegate.h
//  ObjC_ScrollviewApp
//
//  Created by Margaret Schroeder on 1/12/18.
//  Copyright © 2018 Margaret Schroeder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

